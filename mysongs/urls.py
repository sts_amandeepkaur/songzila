from django.urls import path
from mysongs import views
app_name = "mysongs"
urlpatterns = [
    path('',views.all_albums,name="all_albums"),
    path('single_album',views.single_album,name="single_album"),
    path('filter_songs',views.filter_songs,name="filter_songs"),
    path('registerView',views.registerView,name="registerView"),
    path('uslogin',views.uslogin,name="uslogin"),
    path('uslogout',views.uslogout,name="uslogout"),
    path('blogView',views.blogView,name="blogView"),
    path('blog',views.blog,name="blog"),
    path('contact',views.contact,name="contact"),
    
]